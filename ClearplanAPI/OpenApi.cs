﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;


namespace Clearplan
{
    public class OpenApi
    {
        private string baseHttpUrl = "";
        private string apiKey = "";
        private string devUrl = "https://api.cleardata.io/cldev/v1";
        private string prodUrl = "https://api.cleardata.io/cl/v1";
        private static HttpClient httpClient = new HttpClient();

        public enum Endpoint {
            DEV = 0,
            PROD = 1
        }

        public OpenApi(Endpoint e, string secretKey)
        {
            if (e == Endpoint.DEV)
            {
                baseHttpUrl = devUrl;
            }
            else
            {
                baseHttpUrl = prodUrl;
            }
            apiKey = secretKey;
        }

        public async Task<HttpResponseMessage> PostAccount(Account account)
        {
            string postUrl = baseHttpUrl + "/company/account";
            var content = new StringContent(JsonConvert.SerializeObject(account), Encoding.UTF8, "application/json");
            var req = new HttpRequestMessage() { RequestUri = new Uri(postUrl), Method = HttpMethod.Post, Content = content };
            req.Headers.Add("x-api-key", apiKey);
            HttpResponseMessage res = await httpClient.SendAsync(req);
            return res;
        }

        public async Task<HttpResponseMessage> DeleteAccount(string agentCompanyId, string vin, string accountNumber, string reason)
        {
            string deleteUrl = baseHttpUrl + String.Format("/company/{0}/account/{1}/{2}", agentCompanyId, vin, accountNumber);
            var values = new Dictionary<string, string>();
            values.Add("reason", reason);
            var content = new FormUrlEncodedContent(values);
            var req = new HttpRequestMessage() { RequestUri = new Uri(deleteUrl), Method = HttpMethod.Delete, Content = content };
            req.Headers.Add("x-api-key", apiKey);
            HttpResponseMessage res = await httpClient.SendAsync(req);
            return res;
        }

        public async Task<HttpResponseMessage> PostAddress(string agentCompanyId, string vin, Address address)
        {
            string postUrl = baseHttpUrl + String.Format("/company/{0}/account/{1}/address", agentCompanyId, vin);
            var content = new StringContent(JsonConvert.SerializeObject(address), Encoding.UTF8, "application/json");
            var req = new HttpRequestMessage() { RequestUri = new Uri(postUrl), Method = HttpMethod.Post, Content = content };
            req.Headers.Add("x-api-key", apiKey);
            HttpResponseMessage res = await httpClient.SendAsync(req);
            return res;
        }

        public async Task<HttpResponseMessage> DeleteAddress(string agentCompanyId, string vin, string addressId)
        {
            string deleteUrl = baseHttpUrl + String.Format("/company/{0}/account/{1}/address/{2}", agentCompanyId, vin, addressId);
            var req = new HttpRequestMessage() { RequestUri = new Uri(deleteUrl), Method = HttpMethod.Delete };
            req.Headers.Add("x-api-key", apiKey);
            HttpResponseMessage res = await httpClient.SendAsync(req);
            return res;
        }

        public async Task<HttpResponseMessage> PostPlate(string agentCompanyId, string vin, Plate plate)
        {
            string postUrl = baseHttpUrl + String.Format("/company/{0}/account/{1}/plate", agentCompanyId, vin);
            var content = new StringContent(JsonConvert.SerializeObject(plate), Encoding.UTF8, "application/json");
            var req = new HttpRequestMessage() { RequestUri = new Uri(postUrl), Method = HttpMethod.Post, Content = content };
            req.Headers.Add("x-api-key", apiKey);
            HttpResponseMessage res = await httpClient.SendAsync(req);
            return res;
        }

        public async Task<HttpResponseMessage> PostClient(string agentCompanyId, string vin, Client client)
        {
            string postUrl = baseHttpUrl + String.Format("/company/{0}/account/{1}/client", agentCompanyId, vin);
            var content = new StringContent(JsonConvert.SerializeObject(client), Encoding.UTF8, "application/json");
            var req = new HttpRequestMessage() { RequestUri = new Uri(postUrl), Method = HttpMethod.Post, Content = content };
            req.Headers.Add("x-api-key", apiKey);
            HttpResponseMessage res = await httpClient.SendAsync(req);
            return res;
        }

        
    }

}
