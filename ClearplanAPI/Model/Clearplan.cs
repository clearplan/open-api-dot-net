﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Clearplan
{
    public class Account
    {
        public string accountNumber { get; set; }
        public int accountType { get; set; }
        public string agentCompanyId { get; set; }
        public string orderDate { get; set; }
        public Vehicle vehicle { get; set; }
        public Lien lien { get; set; }
        public Client client { get; set; }
        public Debtor debtor { get; set; }
        public Cosigner cosigner { get; set; }
        public List<Address> addresses { get; set; }
        public int status { get; set; }
        public string subStatus { get; set; }
        public string externalURL { get; set; }
        public string externalURLAlt1 { get; set; }
        public string externalURLAlt2 { get; set; }
    }
    public class Vehicle
    {
        public string vin { get; set; }
        public int year { get; set; }
        public string make { get; set; }
        public string model { get; set; }
        public string color { get; set; }
        public string plates { get; set; }
        public string state { get; set; }
        public string platesExpiration { get; set; }
        public string keyCode1 { get; set; }
        public string keyCode2 { get; set; }
        public string dealerName { get; set; }
        public int mileage { get; set; }
    }

    public class Lien
    {
        public string name { get; set; }
        public string id { get; set; }
    }

    public class Client
    {
        public string name { get; set; }
        public string id { get; set; }
    }

    public class Debtor
    {
        public string name { get; set; }
        public string dob { get; set; }
    }

    public class Cosigner
    {
        public string name { get; set; }
        public string dob { get; set; }
    }

    public class Address
    {
        public string extId { get; set; }
        public string name { get; set; }
        public string unit { get; set; }
        public List<string> phoneNumbers { get; set; }
        public string county { get; set; }
        public int type { get; set; }
        public int priority { get; set; }
        public bool isValid { get; set; }
        public bool ceaseAndDesist { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string singleLine { get; set; }
    }


    public class Plate
    {
        public string name { get; set; }
    }

}
