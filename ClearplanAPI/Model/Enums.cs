﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clearplan
{
    public enum AccountType
    {
        Repossess = 0,
        Voluntary = 1,
        Investigate = 2,
        Transport = 3,
        Tow = 4,
        Store = 5,
        ImpoundRepo = 6,
        ImpoundVoluntary = 7,
        Pictures = 8,
        FieldVisit = 9,
        Auction = 10,
        Consign = 11, 
        CondReport = 12,
        ConvtoRepo = 13,
        LPRStaging = 14
    }

    public enum AddressType
    {
        Unknown = 0,
        DebtorHome = 1,
        DebtorWork = 2,
        CoDebtorHome = 3,
        CoDebtorWork = 4,
        LandlordAddress = 5, 
        DMVAddress = 6,
        MiscAddress = 7,
        CoDebtorPrevious = 8,
        DebtorPrevious = 9,
        Reference = 10,
        ThirdPartyAddress = 11,
        PickupAddress = 12,
        PhoneNumber = 13,
        LPRLocation = 14,
        DRNLocation = 15
    }

    public enum AccountStatus
    {
        /*Unassigned = 0,
        NewFromClient = 1,*/
        Open = 2, //currentyly clearplan's API is only accepting Open
        /*Hold = 3,
        DeclinedbyAgent = 4,
        PendingClose = 5,
        PendingOnHold = 6,
        Repossessed = 7,
        Resolved = 8,
        Auction = 9, 
        ChargedOff = 10,
        Completed = 11,
        Closed = 12*/
    }

    public class DeleteReason
    {
        public static string REPOSSESSED = "repossessed";
        public static string CLOSED = "closed";
        public static string NEED_INFO = "need_info";
        public static string PENDING = "pending";
    }
    
}
