﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Clearplan;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;


namespace TestApp
{
    public class Program
    {
        public static OpenApi openApi = new OpenApi(OpenApi.Endpoint.DEV, "your_dev_or_prod_key_here");

        public static string vin = "WDC0J6EB6JF305621";
        public static string agentCompanyId = "12345";
        public static string accountNumber = "CD000000012345";
        public static string addressId2 = "CDADD002";

        public static void Main(string[] args)
        {
            TestPostAccount().Wait();
            TestPostAddress().Wait();
            TestDeleteAddress().Wait();
            TestPostPlate().Wait();
            TestPostClient().Wait();
            TestDeleteAccount().Wait();
        }
        
        public static async Task TestPostAccount()
        {

            Console.WriteLine("Starting Post Account");
            Account acc = new Account();
            Vehicle v = new Vehicle();//this object is required
            Lien l = new Lien();//this object is required
            Client c = new Client();//this object is required
            Debtor d = new Debtor();//this object is required
            List<Address> addresses = new List<Address>();//at least one address is required
            Address address1 = new Address();
            acc.accountNumber = accountNumber; //this field is required
            acc.accountType = (int)Clearplan.AccountType.Repossess; //this field is required
            acc.agentCompanyId = agentCompanyId; //this field is required

            //you also need to format the order date string in iso8601 format i.e. "2017-11-03T09:52:45-07:00"
            //the easiest way to do that if your orderDate is a DateTime is to format it with "o"
            acc.orderDate = DateTime.UtcNow.ToString("o"); ;//this field is required

            v.vin = vin; //this field is required
            v.year = 2017;
            v.make = "Mercedes";
            v.model = "amg glc 43";
            v.color = "gray";
            l.name = "CD Lien Bank";
            l.id = "CD1111"; //this field is required
            c.name = "CD Client";
            c.id = "CD2222";//this field is required
            d.name = "John Smith";//this field is required
            address1.extId = "CDADD001"; //one address is required
            address1.type = (int)Clearplan.AddressType.DebtorHome;
            address1.isValid = true;
            address1.singleLine = "9 Greg Street Sparks NV 89431"; //optional to send street in 1 line
            address1.name = "Work 1"; //or you can send address, city, state, zip
            address1.address = "9 Greg Street";
            address1.city = "Sparks";
            address1.state = "NV";
            address1.zip = "89431";
            acc.status = (int)Clearplan.AccountStatus.Open; //this field is required - must pass 2 for 'Open' right now
            acc.subStatus = "optional, unused right now";
            acc.externalURL = "https://www.google.com"; //this can be a link back to your portal, clearplan will display and hyperlink it in account details
            acc.vehicle = v;
            acc.client = c;
            acc.lien = l;
            addresses.Add(address1);
            acc.addresses = addresses;
            acc.debtor = d;
            HttpResponseMessage res = await openApi.PostAccount(acc);
            Console.WriteLine((int)res.StatusCode);
            string responseBody = await res.Content.ReadAsStringAsync();
            Console.WriteLine(responseBody);
            Console.WriteLine("Post Account Done");
            Console.WriteLine("Press Enter To Start Next Test");
            Console.ReadLine();
        }

        public static async Task TestDeleteAccount()
        {
            Console.WriteLine("Starting Delete Account");
            HttpResponseMessage res = await openApi.DeleteAccount(agentCompanyId, vin, accountNumber, Clearplan.DeleteReason.CLOSED);
            Console.WriteLine((int)res.StatusCode);
            string responseBody = await res.Content.ReadAsStringAsync();
            Console.WriteLine(responseBody);
            Console.WriteLine("Delete Account Done");
            Console.WriteLine("Press Enter To Start Next Test");
            Console.ReadLine();
        }

        public static async Task TestPostAddress()
        {
            Address address2 = new Address();
            address2.extId = addressId2; //one address is required
            address2.type = (int)Clearplan.AddressType.DebtorWork;
            address2.isValid = true;
            address2.name = "Work"; //or you can send address, city, state, zip
            address2.address = "1628 Bluehaven Drive";
            address2.city = "Sparks";
            address2.state = "NV";
            address2.zip = "89434";
            Console.WriteLine("Starting Post Address");
            HttpResponseMessage res = await openApi.PostAddress(agentCompanyId, vin, address2);
            Console.WriteLine((int)res.StatusCode);
            string responseBody = await res.Content.ReadAsStringAsync();
            Console.WriteLine(responseBody);
            Console.WriteLine("Post Address Done");
            Console.WriteLine("Press Enter To Start Next Test");
            Console.ReadLine();
        }

        public static async Task TestDeleteAddress()
        {
            Console.WriteLine("Starting Delete Address");
            HttpResponseMessage res = await openApi.DeleteAddress(agentCompanyId, vin, addressId2);
            Console.WriteLine((int)res.StatusCode);
            string responseBody = await res.Content.ReadAsStringAsync();
            Console.WriteLine(responseBody);
            Console.WriteLine("Delete Address Done");
            Console.WriteLine("Press Enter To Start Next Test");
            Console.ReadLine();
        }

        public static async Task TestPostPlate()
        {
            Plate p = new Plate();
            p.name = "NEW-PLATE";
            Console.WriteLine("Starting Post Plate");
            HttpResponseMessage res = await openApi.PostPlate(agentCompanyId, vin, p);
            Console.WriteLine((int)res.StatusCode);
            string responseBody = await res.Content.ReadAsStringAsync();
            Console.WriteLine(responseBody);
            Console.WriteLine("Post Plate Done");
            Console.WriteLine("Press Enter To Start Next Test");
            Console.ReadLine();
        }

        public static async Task TestPostClient()
        {
            Client c = new Client();
            c.name = "NEW-CLIENT";
            c.id = "CD3333";
            Console.WriteLine("Starting Post Client");
            HttpResponseMessage res = await openApi.PostClient(agentCompanyId, vin, c);
            Console.WriteLine((int)res.StatusCode);
            string responseBody = await res.Content.ReadAsStringAsync();
            Console.WriteLine(responseBody);
            Console.WriteLine("Post Client Done");
            Console.WriteLine("Press Enter To Start Next Test");
            Console.ReadLine();
        }
    }
}
