﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports Clearplan
Imports System.Threading.Tasks
Imports System.Net
Imports System.Net.Http

Namespace TestApp

    Public Class Program

        Public Shared openApi As OpenApi = New OpenApi(OpenApi.Endpoint.DEV, "your_dev_or_prod_key_here")

        Public Shared vin As String = "WDC0J6EB6JF305621"

        Public Shared agentCompanyId As String = "12345"

        Public Shared accountNumber As String = "CD000000012345"

        Public Shared addressId2 As String = "CDADD002"

        Public Shared Sub Main(ByVal args() As String)
            Program.TestPostAccount.Wait()
            Program.TestPostAddress.Wait()
            Program.TestDeleteAddress.Wait()
            Program.TestPostPlate.Wait()
            Program.TestPostClient.Wait()
            Program.TestDeleteAccount.Wait()
        End Sub

        Public Shared Async Function TestPostAccount() As Task
            Console.WriteLine("Starting Post Account")
            Dim acc As Account = New Account
            Dim v As Vehicle = New Vehicle
            'this object is required
            Dim l As Lien = New Lien
            'this object is required
            Dim c As Client = New Client
            'this object is required
            Dim d As Debtor = New Debtor
            'this object is required
            Dim addresses As List(Of Address) = New List(Of Address)
            'at least one address is required
            Dim address1 As Address = New Address
            acc.accountNumber = accountNumber
            'this field is required
            acc.accountType = CType(Clearplan.AccountType.Repossess, Integer)
            'this field is required
            acc.agentCompanyId = agentCompanyId
            'this field is required
            'you also need to format the order date string in iso8601 format i.e. "2017-11-03T09:52:45-07:00"
            'the easiest way to do that if your orderDate is a DateTime is to format it with "o"
            acc.orderDate = DateTime.UtcNow.ToString("o")

            'this field is required
            v.vin = vin
            'this field is required
            v.year = 2017
            v.make = "Mercedes"
            v.model = "amg glc 43"
            v.color = "gray"
            l.name = "CD Lien Bank"
            l.id = "CD1111"
            'this field is required
            c.name = "CD Client"
            c.id = "CD2222"
            'this field is required
            d.name = "John Smith"
            'this field is required
            address1.extId = "CDADD001"
            'one address is required
            address1.type = CType(Clearplan.AddressType.DebtorHome, Integer)
            address1.isValid = True
            address1.singleLine = "9 Greg Street Sparks NV 89431"
            'optional to send street in 1 line
            address1.name = "Work 1"
            'or you can send address, city, state, zip
            address1.address = "9 Greg Street"
            address1.city = "Sparks"
            address1.state = "NV"
            address1.zip = "89431"
            acc.status = CType(Clearplan.AccountStatus.Open, Integer)
            'this field is required - must pass 2 for 'Open' right now
            acc.subStatus = "optional, unused right now"
            acc.externalURL = "https://www.google.com"
            'this can be a link back to your portal, clearplan will display and hyperlink it in account details
            acc.vehicle = v
            acc.client = c
            acc.lien = l
            addresses.Add(address1)
            acc.addresses = addresses
            acc.debtor = d
            Dim res As HttpResponseMessage = Await openApi.PostAccount(acc)
            Console.WriteLine(CType(res.StatusCode, Integer))
            Dim responseBody As String = Await res.Content.ReadAsStringAsync
            Console.WriteLine(responseBody)
            Console.WriteLine("Post Account Done")
            Console.WriteLine("Press Enter To Start Next Test")
            Console.ReadLine()
        End Function

        Public Shared Async Function TestDeleteAccount() As Task
            Console.WriteLine("Starting Delete Account")
            Dim res As HttpResponseMessage = Await openApi.DeleteAccount(agentCompanyId, vin, accountNumber, Clearplan.DeleteReason.CLOSED)
            Console.WriteLine(CType(res.StatusCode, Integer))
            Dim responseBody As String = Await res.Content.ReadAsStringAsync
            Console.WriteLine(responseBody)
            Console.WriteLine("Delete Account Done")
            Console.WriteLine("Press Enter To Start Next Test")
            Console.ReadLine()
        End Function

        Public Shared Async Function TestPostAddress() As Task
            Dim address2 As Address = New Address
            address2.extId = addressId2
            'one address is required
            address2.type = CType(Clearplan.AddressType.DebtorWork, Integer)
            address2.isValid = True
            address2.name = "Work"
            'or you can send address, city, state, zip
            address2.address = "1628 Bluehaven Drive"
            address2.city = "Sparks"
            address2.state = "NV"
            address2.zip = "89434"
            Console.WriteLine("Starting Post Address")
            Dim res As HttpResponseMessage = Await openApi.PostAddress(agentCompanyId, vin, address2)
            Console.WriteLine(CType(res.StatusCode, Integer))
            Dim responseBody As String = Await res.Content.ReadAsStringAsync
            Console.WriteLine(responseBody)
            Console.WriteLine("Post Address Done")
            Console.WriteLine("Press Enter To Start Next Test")
            Console.ReadLine()
        End Function

        Public Shared Async Function TestDeleteAddress() As Task
            Console.WriteLine("Starting Delete Address")
            Dim res As HttpResponseMessage = Await openApi.DeleteAddress(agentCompanyId, vin, addressId2)
            Console.WriteLine(CType(res.StatusCode, Integer))
            Dim responseBody As String = Await res.Content.ReadAsStringAsync
            Console.WriteLine(responseBody)
            Console.WriteLine("Delete Address Done")
            Console.WriteLine("Press Enter To Start Next Test")
            Console.ReadLine()
        End Function

        Public Shared Async Function TestPostPlate() As Task
            Dim p As Plate = New Plate
            p.name = "NEW-PLATE"
            Console.WriteLine("Starting Post Plate")
            Dim res As HttpResponseMessage = Await openApi.PostPlate(agentCompanyId, vin, p)
            Console.WriteLine(CType(res.StatusCode, Integer))
            Dim responseBody As String = Await res.Content.ReadAsStringAsync
            Console.WriteLine(responseBody)
            Console.WriteLine("Post Plate Done")
            Console.WriteLine("Press Enter To Start Next Test")
            Console.ReadLine()
        End Function

        Public Shared Async Function TestPostClient() As Task
            Dim c As Client = New Client
            c.name = "NEW-CLIENT"
            c.id = "CD3333"
            Console.WriteLine("Starting Post Client")
            Dim res As HttpResponseMessage = Await openApi.PostClient(agentCompanyId, vin, c)
            Console.WriteLine(CType(res.StatusCode, Integer))
            Dim responseBody As String = Await res.Content.ReadAsStringAsync
            Console.WriteLine(responseBody)
            Console.WriteLine("Post Client Done")
            Console.WriteLine("Press Enter To Start Next Test")
            Console.ReadLine()
        End Function
    End Class
End Namespace